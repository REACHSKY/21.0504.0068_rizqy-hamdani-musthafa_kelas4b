import 'package:flutter/material.dart';

void main() {
  runApp(const FungsiUtama());
}

class FungsiUtama extends StatelessWidget {
  const FungsiUtama({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("mengHello World"),
      ),
      body: Center(
        child: Text("Hewwo World UwU"),
      ),
    ));
  }
}
